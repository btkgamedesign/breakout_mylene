﻿using UnityEngine;


public class Floor : MonoBehaviour 
{
	public paddle1 paddle;

	void OnCollisionEnter(Collision collision) 
    {
		paddle.NewPuck();
		Destroy(collision.gameObject);
	}
}

