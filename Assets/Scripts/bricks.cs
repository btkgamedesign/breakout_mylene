﻿using UnityEngine;
using DG.Tweening;

public class bricks : MonoBehaviour 
{
	public CameraEffect cameraEffect;
	
	void OnCollisionEnter(Collision collision) 
    {
		GetComponent<AudioSource>().Play();
		collision.gameObject.GetComponent<Puck>().ShakeScale();
		cameraEffect.Shake();
		
		enabled = false;
		GetComponent<Renderer>().enabled = false;
		GetComponent<Collider>().enabled = false;
	}

}


