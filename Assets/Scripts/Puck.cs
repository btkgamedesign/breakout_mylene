﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Puck : MonoBehaviour
{
   public Rigidbody rb; 
   public bool launched;
   

    const float minSpeed = 8f;
    const float maxSpeed = 12f;

   void FixedUpdate()
    {
        if (launched)
        {
            float speed = rb.velocity.magnitude;
            
            if (speed < minSpeed)
            {
                rb.velocity *= 10f / minSpeed;
            }
              else if(speed > maxSpeed) {
                rb.velocity *= 10f / maxSpeed;
            }
        }
    }

    public void ShakeScale()
    {
        transform.DOShakeScale(.25f, 1f, 5, 50f, true);
    }

  }

