﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class fallIntoPlace : MonoBehaviour
{

    void Start()
    {
        float targetY = transform.position.y;
        transform.Translate(new Vector3(0, 5, 0));
        transform.DOMoveY(targetY, Random.Range(0.5f, 1)).SetEase(Ease.OutBounce);
    }   
}
